#
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

import psycopg2
import sys
import re

# Arguments
name_bd = str(sys.argv[1])
server_bd = str(sys.argv[2])
passwd = str(sys.argv[3])
user = str(sys.argv[4])
rtype = str(sys.argv[5])

def get_conn(name_bd, user, passwd, server_bd):
    conn = psycopg2.connect(dbname=name_bd, user=user, password=passwd, host=server_bd)
    cur = conn.cursor()
    return conn, cur

def get_all_element(rtype, cur):
    cur.execute("SELECT id, identifier, name, path, synonym FROM element WHERE type = '"+rtype+"'")
    res = cur.fetchall()
    return res

def get_relation(id, cur):
    cur.execute("SELECT r.id_source FROM relation r, element e WHERE r.id_element = e.id AND r.id_element IN (select id from element where path like '%/" + id + "/%' or identifier = '" + id + "')")
    res = cur.fetchall()
    return res

def insert_list(rtype, name, path, cur):
    insert = "INSERT INTO list_"+rtype
    if rtype != 'habitat':
        insert += "_taxon"
    insert += " (name, path) VALUES ('"+name+"', '"+path+"')"
    cur.execute(insert)
    sql.write(insert+";\n")
    conn.commit()

# Database connection
conn, cur = get_conn(name_bd, user, passwd, server_bd)

with open('List_'+rtype+'.sql', 'w') as sql:
    all_element = get_all_element(rtype, cur)
    for element in all_element:
        relations = get_relation(element[1], cur)

        if len(relations) > 0:
            name = element[2] ; name = re.sub("'", "''", name)
            path = element[3]
            insert_list(rtype, name, path, cur)
            if element[4] != '':
                for synonym in element[4].split(', '):
                    synonym = re.sub("'", "''", synonym)
                    insert_list(rtype, synonym, path, cur)
