#
# Copyright 2022 Sandra Dérozier (INRAE)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

import psycopg2
import sys

# Arguments
name_bd = str(sys.argv[1])
server_bd = str(sys.argv[2])
passwd = str(sys.argv[3])
user = str(sys.argv[4])
v_flo = str(sys.argv[5])
v_ncbi = str(sys.argv[6])
v_onto = str(sys.argv[7])
v_pubmed = str(sys.argv[8])
v_dsmz = str(sys.argv[9])
v_gb = str(sys.argv[10])
v_cirmbia = str(sys.argv[11])
v_cirmlevures = str(sys.argv[12])
v_cirmcfbp = str(sys.argv[13])

# DB Connection
conn = psycopg2.connect(dbname=name_bd, user=user, password=passwd, host=server_bd)
cur = conn.cursor()

# Output
output = open("Versions.txt", "w")

# Insert
req = "INSERT INTO v_source (omnicrobe, taxonomy, ontobiotope, pubmed, dsmz, genbank, cirmbia, cirmlevures, cirmcfbp) VALUES ('"+v_flo+"', '"+v_ncbi+"', '"+v_onto+"', '"+v_pubmed+"', '"+v_dsmz+"', '"+v_gb+"', '"+v_cirmbia+"', '"+v_cirmlevures+"', '"+v_cirmcfbp+"')"
print("Requete :", req)
cur.execute(req)
conn.commit()

output.write("Insertion query:\n\n")
output.write(req)
